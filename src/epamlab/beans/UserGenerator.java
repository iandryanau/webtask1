package epamlab.beans;

import java.util.ArrayList;
import java.util.List;

public class UserGenerator {
	private static final int LENGTH = 10;	
	private static String[] NAME = {"Fedor", "Artur", "Nikita", "Ivan", "Alex", "Dmitry", "Oleg", "Toha", "Stepan", "Sergei"};
	private static String[] LASTNAME = {"Kyzmin", "Torfenkov", "Ivanov", "Stolerov", "Folkonski", "Turin", "Sirkov", "Donskoi", "Tolstoi", "Kypala"};
	private static String[] LOGIN = {"ffgrry56", "green4gold", "doodleJoy", "iv@n111", "yar4skou", "jumplen", "kusokkk", "vodddg", "potruu", "ascyd4curb"};
	
	public static List<User> generate(int userNumber){
		
		List<User> users = new ArrayList<User>(userNumber);
		for (int i = 0; i < userNumber; i++) {
			String firstname = NAME[(int) (Math.random() * LENGTH)];
			String lastname = LASTNAME[(int) (Math.random() * LENGTH)];
			String login = LOGIN[(int) (Math.random() * LENGTH)];
			users.add(new User(firstname, lastname, login));
		}
		return users;
	}

}
