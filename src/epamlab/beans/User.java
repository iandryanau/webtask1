package epamlab.beans;

public class User {
	private String name;
	private String lastname;
	private String login;
	
	public User (){	
	}
	
	public User(String name, String lastname,String login){
		this.name = name;
		this.lastname = lastname;
		this.login = login;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public String toString(){
		return name + ";" + lastname + ";" + login;
	}

}
