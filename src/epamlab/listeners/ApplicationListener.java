package epamlab.listeners;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import epamlab.beans.User;
import epamlab.beans.UserGenerator;
import epamlab.db.DBManager;

@WebListener
public class ApplicationListener implements ServletContextListener, ServletContextAttributeListener {

	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		System.out.println("Server info: " + context.getServerInfo());
		System.out.println("Servlet context name: " + context.getServletContextName());

		Iterator<String> iterator = context.getServletRegistrations().keySet().iterator();
		System.out.println("Available servlets: ");
		while (iterator.hasNext()) {
			System.out.println("   Servlet: " + iterator.next());
		}

		List<User> users = UserGenerator.generate(Integer.parseInt(context.getInitParameter("usersNumber")));
		try {
			DBManager.addUsers(users);
		} catch (SQLException e) {
			System.err.println(e);
		}
		context.setAttribute("users", users);

	}

	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("Context destroyed");
	}

	public void attributeReplaced(ServletContextAttributeEvent scae) {
		String name = scae.getName();
		System.out.println("Attribute replaced: " + name + " - " + scae.getValue() + " with "
				+ scae.getServletContext().getAttribute(name));
	}

	public void attributeAdded(ServletContextAttributeEvent scae) {
		
		System.out.println("Attribute added: " + scae.getName() + " - " + scae.getValue());
	}

	public void attributeRemoved(ServletContextAttributeEvent scae) {
		System.out.println("Attribute removed: " + scae.getName() + " - " + scae.getValue());
	}

}
