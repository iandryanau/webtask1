package epamlab.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import epamlab.beans.User;
import epamlab.db.DBManager;

@WebServlet("/showUsers")
public class ThatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private boolean isAvailable;

	@Override
	public void init() throws ServletException {
		isAvailable = !Boolean.parseBoolean(getServletContext().getInitParameter("isAvailable"));
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext context = getServletContext();
		if (!isAvailable) {
			isAvailable = true;
			throw new UnavailableException("Something's wrong",
					Integer.parseInt(context.getInitParameter("errorTime")));
		}
		List<User> users = null;
		try {
			users = DBManager.getUsers();
		} catch (ClassCastException e) {
			System.out.println("User list isn't correct!!!");
		} catch (SQLException e) {
			System.out.println("SQL Exception in DBManager.getUsers()!!!");
		}
		
		PrintWriter pw = response.getWriter();
		pw.println("<!DOCTYPE html>" + " + <HTML>\n" + "<HEAD><TITLE>User Page</TITLE></HEAD>\n" + "<BODY>\n" + "<ul>" + "<h3>User list:</h3>\n");
		if (users == null) {
			System.out.println("list is empty!");
		} else {
			for (User user : users) {
				pw.println("<il>" + user + "</il>\n" + "<br>");
				System.out.println("~~" + user);
			}
			pw.println("</ul>");
		}
		pw.println("</BODY></HTML>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
