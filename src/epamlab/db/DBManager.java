package epamlab.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import epamlab.beans.User;

public class DBManager {
	private final static String CLASS_PATH = "com.mysql.jdbc.Driver";
	private final static String DB_CONNECTION = "jdbc:mysql://localhost:3306/userlist";
	private final static String LOGIN_PASS = "root";
	private final static String ADD_USERS = "INSERT INTO `users`(`name`, `lastname`, `login`) VALUES (?, ?, ?)";
	private final static String DELETE_USERS = "DELETE FROM `users`";
	private final static String GET_USERS = "SELECT * FROM `users`";
	private static PreparedStatement ps = null;
	private static Connection cn = null;
	private static Statement st = null;
	private static final int RS_NAME = 1;
	private static final int RS_LASNAME = 2;
	private static final int RS_LOGIN = 3;

	public static void addUsers(List<User> users) throws SQLException {
		try {
			Class.forName(CLASS_PATH);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		cn = DriverManager.getConnection(DB_CONNECTION, LOGIN_PASS, LOGIN_PASS);
		st = cn.createStatement();
		st.execute(DELETE_USERS);

		ps = cn.prepareStatement(ADD_USERS);
		for (User user : users) {
			ps.setString(1, user.getName());
			ps.setString(2, user.getLastname());
			ps.setString(3, user.getLogin());
			ps.executeUpdate();
		}
	}
	
	public static List<User> getUsers() throws SQLException {
		List<User> users = new ArrayList<User>();
		User user = null;
		
		ResultSet rs = st.executeQuery(GET_USERS);
		while (rs.next()){
			user = new User();
			user.setName(rs.getString(RS_NAME));
			user.setLastname(rs.getString(RS_LASNAME));
			user.setLogin(rs.getString(RS_LOGIN));
			users.add(user);
		}
		return users;
	}
}
